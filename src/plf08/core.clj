(ns plf08.core
  (:gen-class))

(defn verificación
  [x caracteres]
  (letfn [(f [x] (contains? caracteres x))
          (g [x] (or (<= 65 (int x) 90) (<= 193 (int x) 218) (f x)))
          (h [x] (into #{} (map g x)))]
    (= 1 (count (h x)))))

(defn posiciones
  [x caracteres]
  (letfn [(f [x] (if (or (<= 65 (int x) 90) (<= 193 (int x) 218)) (char (+ (int x) 32)) x))
          (g [x] (map vector (map caracteres (map f x)) (range 0 (count x))))]
    (g x)))

(defn rectángulo
  [texto y x]
  (letfn [(f [xs] (take y xs))
          (g [xs] (drop y xs))
          (h [xs] (into [] xs))]
    (h (map h (map f (take x (iterate g texto)))))))

(defn cifrar-t
  [clave texto caracteres]
  (letfn [(f [x] (map (fn [xs] (get xs x)) (rectángulo texto (count clave) (/ (count texto) (count clave)))))
          (g [] (map f (into [] (map last (sort (posiciones clave caracteres))))))
          (h [] (apply concat (g)))]
    (apply str (h))))

(defn descifrar-t
  [clave texto caracteres]
  (letfn [(f [xs] (map (fn [x] (get xs x)) (map last (sort (posiciones clave caracteres)))))
          (g [x] (map (fn [xs] (get xs x)) (into [] (f (rectángulo texto (/ (count texto) (count clave)) (count clave))))))
          (h [] (map g (into [] (range 0 (/ (count texto) (count clave))))))
          (i [] (apply concat (h)))]
    (apply str (i))))

;;--------------------------------------------------------------------------------------------------
;;CUADRADO DE POLIBIO
;;--------------------------------------------------------------------------------------------------
(defn posiciones-cp
  [x caracteres]
  (letfn [(h [x] (if (nil? (caracteres x)) x (caracteres x)))]
    (map h x)))

(defn coordenadas
  [xs ajedrez]
  (letfn [(f [x] (apply str (map conj (vector (ajedrez (quot x 10)) (ajedrez (mod x 10))))))
          (g [x] (if (number? x) (f x) x))]
    (apply str (map g xs))))

(defn agrega
  [xs ajedrez]
  (letfn [(f [x] (if (nil? (ajedrez x)) (str x 0) x))]
    (apply str (concat (map f xs)))))

(defn coordenadas-inverso
  [xs ajedrez]
  (letfn [(f [x] (if (nil? (ajedrez x)) x (ajedrez x)))
          (g [x] (rectángulo (agrega x ajedrez) 2 (quot (count (agrega x ajedrez)) 2)))
          (h [x] (into [] (map f x)))]
    (map h (g xs))))

(defn cifrar-cp
  [texto caracteres ajedrez]
  (letfn [(f [x] (posiciones-cp x (into {} (map vector caracteres (concat (range 0 75))))))]
    (coordenadas (f texto) (into {} (map vector (concat (range 0 10)) ajedrez)))))

(defn descifrar-cp
  [texto caracteres ajedrez]
  (letfn [(f [x] (coordenadas-inverso x (into {} (map vector ajedrez (concat (range 0 10))))))
          (g [x] (if (char? (first x)) (first x) (+ (* (first x) 10) (last x))))
          (h [x] (if (char? x) x ((into {} (map vector (concat (range 0 75)) caracteres)) x)))]
    (apply str (map h (into [] (map g (f texto)))))))

(defn transposición
  [x y xs ys caracteres]
  (spit ys (x y (slurp xs) (into {} (map vector caracteres (concat (range 0 75)))))))

(defn cuadrado-de-polibio
  [operación entrada salida coordenada caracteres]
  (spit salida (operación (slurp entrada) caracteres coordenada)))

;;Funciona sin problemas
(defn principal
  [metodo operación entrada salida & [clave]]
  (let [coordenada [\♜ \♞ \♝ \♛ \♚ \♖ \♘ \♗ \♕ \♔]
        caracteres [\a \á \b \c \d \e \é \f \g \h \i \í \j \k \l \m \n \ñ \o \ó \p \q \r \s \t \u \ú \ü \v \w \x \y \z \0 \1 \2 \3 \4
                    \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \= \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9]]
    (cond
      (= "transposición" metodo)
      (if (verificación clave (into {} (map vector caracteres (concat (range 0 75)))))
        (transposición operación clave entrada salida caracteres)
        "ERROR: La palabra clave es incorrecta, no cumple con las características necesarias, verifique que no esté utilizando símbolos incorrectos")
      (= "cuadrado-de-polibio" metodo) (cuadrado-de-polibio operación entrada salida coordenada caracteres)
      :else "No existe el método o ha introducido mal el nombre")))

(principal "transposición" cifrar-t "resources/entrada.txt" "resources/salida.txt" "cAt")
(principal "transposición" descifrar-t "resources/entrada.txt" "resources/salida.txt" "cAt")
(principal "cuadrado-de-polibio" cifrar-cp "resources/entrada.txt" "resources/salida.txt")

(defn -main
  [metodo operación entrada salida &[clave]]
  (let [coordenada [\♜ \♞ \♝ \♛ \♚ \♖ \♘ \♗ \♕ \♔]
        caracteres [\a \á \b \c \d \e \é \f \g \h \i \í \j \k \l \m \n \ñ \o \ó \p \q \r \s \t \u \ú \ü \v \w \x \y \z \0 \1 \2 \3 \4
                    \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \= \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9]]
    ;;No quisiera utilizar tantas sentencias condicionales pero no encuentro la manera de dar como parámetro el nombre de la función y que no me devuelva
    ;;el siguiente error:
    ;;Syntax error (ClassCastException) compiling at (C:\Users\alega\AppData\Local\Temp\form-init17328055141268260504.clj:1:109).
    ;;class java.lang.String cannot be cast to class clojure.lang.IFn (java.lang.String is in module java.base of loader 'bootstrap'; clojure.lang.IFn is in unnamed module of loader 'app')
    ;;¿Hay forma de solucionarlo que no sea como lo "solucioné" a continuación? Porque si no lo hago desde terminal (como en el caso de principal) si funciona
    (cond 
      (= "transposición" metodo) 
      (if (verificación clave (into {} (map vector caracteres (concat (range 0 75)))))
        (cond (= operación "cifrar") (transposición cifrar-t clave entrada salida caracteres)
              (= operación "descifrar") (transposición descifrar-t clave entrada salida caracteres)
              :else (println "No existe la operación " operación))
        (println "ERROR: La palabra clave es incorrecta, no cumple con las características necesarias, verifique que no esté utilizando símbolos incorrectos"))
      (= "cuadrado-de-polibio" metodo) (cuadrado-de-polibio operación entrada salida coordenada caracteres)
      :else (println "No existe el método o ha introducido mal el nombre"))))